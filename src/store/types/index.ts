export interface Article {
  firstName: string;
  lastName: string;
  email: string;
  phone?: string;
}

export interface ArticleStateState {
  article?: Article;
  error: boolean;
}