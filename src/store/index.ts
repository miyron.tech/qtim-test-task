import { createStore, Store } from 'vuex';
import axios, { type AxiosResponse } from 'axios';

interface RootState {}

const store: Store<RootState> = createStore({
  state: {
    itemsCount:8,
    articles: []
  },
  actions: {
    fetchData({commit}): Promise<any> {
      return new Promise((resolve, reject) => {
        axios.get('https://6082e3545dbd2c001757abf5.mockapi.io/qtim-test-work/posts/').then(response => {
          console.log(response)
          commit('addArticles',response.data)
        })
      })
    }
  },
  mutations: {
    addArticles(state, articles){
      state.articles = articles
    }
  },
  getters: {
    getArticles: state => page => {
      page = page < 1? 1: page;
      const startIndex = (page - 1) * state.itemsCount; // Index where the second set starts (inclusive)
      const endIndex = page * state.itemsCount; // Index where the second set ends (exclusive)
      return state.articles.slice(startIndex, endIndex)
    } 
  },
  modules: {},
});

export default store;
