import { createRouter, createWebHistory } from 'vue-router'
/* LAYOUTS */
import MainLayout from '../layout/MainLayout.vue'
/* VIEWS */
import ArticleList from '../views/ArticleList.vue'
import ArticleDetail from '../views/ArticleDetail.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: MainLayout,
      children:[
        {
          name: 'ArticleList',
          path: '/',
          component: ArticleList
        },
        {
          name: 'ArticleDetail',
          path: '/article/:id',
          component: ArticleDetail
        }
      ]
    }
  ]
})

export default router
