import './assets/main.css'
import 'normalize.css';
import { createApp } from 'vue';
import App from './App.vue';
import VueAxios from 'vue-axios';
import router from './router';
import axios from './axios/index.ts';
import store from './store';

const app = createApp(App);

app.use(store);
app.use(router);
app.use(VueAxios, axios);
app.mount('#app');

